# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
  config :badge_client, BadgeClientWeb.Endpoint,
  server: true,
  url: [host: "localhost"],
  secret_key_base: "g2/bh4LlUyYrJ4FEeR2YNQi8FcupQMuYEgHS/kvJ9M15m8nFHZYmFRBYyzVEvbxG",
  render_errors: [view: BadgeClientWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: BadgeClient.PubSub,
  live_view: [signing_salt: "wZvV2ozOM8B/IPLz/SPZJnBTIQOM8QUm"]

  #
  #config :badge_client, BadgeClientWeb.Endpoint2,
  #pubsub: [name: BadgeClient.PubSub]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
