defmodule BadgeGraphql.Client do
  alias Badgeapi.BadgeService.Stub

  alias Badgeapi.{
    CreateBadgeRequest,
    CreateBadgeResponse,

    GetBadgeRequest,
    GetBadgeRequestAll,
    GetBadgeResponseAll,

    DeleteBadgeRequest,
    DeleteBadgeResponse,

    UpdateBadgeRequest,
    UpdateBadgeResponse

  }

  @url System.get_env("ip")

  #######################################################################
  def create_badge(
        name,
        order,
        video_url,
        image
      ) do
    with {:ok, channel} <- GRPC.Stub.connect(@url) || "localhost:5072",
         {:ok, %CreateBadgeRequest{} = request} <-
          build_request_create_badge(
            name,
            order,
            video_url,
            image
           ),
         {:ok, %CreateBadgeResponse{status: status}} <- Stub.create_badge(channel, request) do
      {:ok, status}
    end
  end

  defp build_request_create_badge(
      name,
      order,
      video_url,
      image
       ),
       do:
         {:ok,
         CreateBadgeRequest.new(
            name: name,
            order: order,
            video_url: video_url,
            image: image
          )}

  def get_all_badges() do

    with {:ok, channel} <- GRPC.Stub.connect(@url) || "localhost:5072",
      {:ok, %GetBadgeRequestAll{} = request} <- build_request_get_badge(),
      {:ok, %GetBadgeResponseAll{badges: badges}} <-
      Stub.get_badge_all(channel, request) do
        badges
    end
  end

  defp build_request_get_badge() do
    {:ok, GetBadgeRequestAll.new()}
  end

  def get_badge(id) do
    with {:ok, channel} <- GRPC.Stub.connect(@url) || "localhost:5072",
         {:ok, %GetBadgeRequest{} = request} <- build_request_get_badge(id),
         {:ok, %GetBadgeResponseAll{badges: badges}} <-
           Stub.get_badge(channel, request) do
            badges
    end
  end

  defp build_request_get_badge(id) do
    {:ok, GetBadgeRequest.new(id: id)}
  end

  def update_badge(
        id,
        name,
        order,
        video_url,
        image
      ) do
    with {:ok, channel} <- GRPC.Stub.connect(@url) || "localhost:5072",
         {:ok, %UpdateBadgeRequest{} = request} <-
           build_request_update_badge(
            id,
            name,
            order,
            video_url,
            image
           ),
         {:ok, %UpdateBadgeResponse{status: status}} <-
           Stub.update_badge(channel, request) do
      {:ok, status}
    end
  end

  defp build_request_update_badge(
        id,
        name,
        order,
        video_url,
        image
       ),
       do:
         {:ok,
          UpdateBadgeRequest.new(
            id: id,
            name: name,
            order: order,
            video_url: video_url,
            image: image
          )}

  def delete_badge(id) do
    with {:ok, channel} <- GRPC.Stub.connect(@url) || "localhost:5072",
         {:ok, %DeleteBadgeRequest{} = request} <- build_request_delete_Badge(id),
         {:ok, %DeleteBadgeResponse{status: status}} <-
           Stub.delete_badge(channel, request) do
      {:ok, status}
    end
  end

  defp build_request_delete_Badge(id), do: {:ok, DeleteBadgeRequest.new(id: id)}


end
