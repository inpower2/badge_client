defmodule BadgeGraphql.GraphQL.Schema do

  use Absinthe.Schema

  alias BadgeGraphql.Client


  object :result do
    field :success, :boolean
  end

  object :badges do

    field :id, :string
    field :name, :string
    field :order, :string
    field :video_url, :string
    # field :video_url , :string
    field :image, :string, default_value: " Empty"


  end


  query do

    @desc "provide nothing, It will return all data
    EX:
    query{
      getAllBadges()
      {
        id
        name
        order

      }
    }"
    field :get_all_badges, list_of(:badges) do
      resolve fn _, _, _ ->
        data = Client.get_all_badges()
        IO.inspect(data, label: "here is data of get_all_badges")
        {:ok, data}
      end
    end

    @desc "provide id, It will return all data against that id
    EX:
    query{
      getBadge(id: '3de78e38-2b33-4218-b741-3f599dbb50c1')
      {
        id
        name
        order

      }
    }"
    field :get_badge, list_of(:badges) do
      arg :id, non_null(:string)
      resolve fn _,%{id: id}, _ ->
        data = Client.get_badge(id)
        IO.inspect(data, label: "here is data of get_badge")
        {:ok, data}
      end
    end
  end

  mutation do


    @desc "provide name, order and id then it will return updated values
    EX:
    mutation{
      updateBadge(
        name: 'test'
        id: '3de78e38-2b33-4218-b741-3f599dbb50c1'
        order: 'Desc'
        image: 'base64 string' // must not be blank
        video_url : '84993c8e-e544-11eb-ba80-0242ac130004'
      )
      {
        id
        image
        video_url
        name
        order
      }
    }"


    field :update_badge, :result do

      arg :id, non_null(:string)
      arg :name, non_null(:string)
      arg :order, non_null(:string)
      # arg :video_url , non_null(:string)
      arg :video_url, non_null(:string)
      arg :image, non_null(:string)

      resolve fn(%{name: name, id: id, order: order, image: image, video_url: video_url }, _context) ->

        badge_update = Client.update_badge(id, name, order, video_url , image)
        case badge_update do
          {:ok, true} ->
            IO.inspect(badge_update, label: "here is data of update_badge")

            {:ok, %{success: false}}
          {:ok, false} ->
            IO.inspect(badge_update, label: "here is data of update_badge")

            {:ok, %{success: true}}
        end
      end
    end

    @desc "provide  id, it will return boolean value True for deleted false for fail to delete
    EX:
    mutation{
      deleteBadge(id: '3de78e38-2b33-4218-b741-3f599dbb50c1'){
        success
      }
    }"


    field :delete_badge, :result do
      arg :id, non_null(:string)

      resolve fn(%{id: id}, _context) ->

        delete_badge = Client.delete_badge(id)
        case delete_badge do
          {:ok, true} ->
            IO.inspect(delete_badge, label: "here is data of delete_badge")

            {:ok, %{success: true}}
          {:ok, false} ->
            IO.inspect(delete_badge, label: "here is data of delete_badge")

            {:ok, %{success: false}}
        end
      end
    end


    @desc "provide name and order  then it will return created values
    EX:
    mutation{
      createBadge(
        name: 'test',
        order: 'Desc'
        image: 'base64 string' // must not be blank
        video_url : '84993c8e-e544-11eb-ba80-0242ac130004'

      ) {
        id
        image
        video_url
        name
        order
      }
    }"


    field :create_badge, :result do

      arg :name, non_null(:string)
      arg :order, non_null(:string)
      # arg :video_url , non_null(:string)
      arg :video_url, non_null(:string)
      arg :image, non_null(:string)


      resolve fn(%{name: name, order: order, video_url: video_url , image: image}, _context) ->

        badge = Client.create_badge(name, order, video_url , image)
        IO.inspect(badge, label: "check for schema in create badge")
        case badge do
          {:ok, true} ->
            {:ok, %{success: true}}
          {:ok, false} ->
            {:ok, %{success: true}}
        end
      end
    end
  end
end
