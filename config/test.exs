use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :badge_client, BadgeClientWeb.Endpoint,
  http: [port: 5073],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
