defmodule BadgeClientWeb.BadgeTest do
  use BadgeClientWeb.ConnCase

  setup do
    Neuron.Config.set(url: "http://localhost:6002/graphiql")
    Neuron.query("""
              mutation
                {
                  createComment(
                    comments: "test"
                    isdeletedbyadmin: true
                    likecount: 2
                    mediaUrl: " "
                    postid: "8c0122a2-e544-11eb-ba80-0242ac130004"
                    status: 1
                    userid: "8c0122a2-e544-11eb-ba80-0242ac130004"
                    userlikes: 3

                  )
                  {
                    success
                  }
                }
                 """)

     {:ok, comment} =
      Neuron.query("""
                query{
                  getComments(postid: "8c0122a2-e544-11eb-ba80-0242ac130004" ){
                    id
                  }
                }
                 """)
        # IO.inspect Enum.at(comment.body["data"]["getComments"], 0)["id"], label: "here is comment id"
        comment_id = Enum.at(comment.body["data"]["getComments"], 0)["id"]
        Neuron.query("""
        mutation{
          createReply(
            commentid: "#{comment_id}"
            isdeletedbyadmin: true
            likecount: 4
            mediaUrl: " "
            postid: "8c0122a2-e544-11eb-ba80-0242ac130004"
            reply: "test"
            status: 1
            userid: "8c0122a2-e544-11eb-ba80-0242ac130004"
            userlikes: 4
          )
          {
            success
          }
        }
     """)
     {:ok, reply} =
      Neuron.query("""
                mutation{
                  getRepyId(commentid: "#{comment_id}")
                  {
                    id
                  }
                }
                 """)
                 reply_id_inserted =  Enum.at(reply.body["data"]["getRepyId"], 0)["id"]
    {:ok, reply_id_inserted: reply_id_inserted, comment_id: comment_id}
  end

  # test "Create comment", %{conn: _conn} do
  #   {:ok, resp} =
  #     Neuron.query("""
  #             mutation
  #               {
  #                 createComment(
  #                   comments: "test"
  #                   isdeletedbyadmin: true
  #                   likecount: 2
  #                   mediaUrl: " "
  #                   postid: "8c0122a2-e544-11eb-ba80-0242ac130004"
  #                   status: 1
  #                   userid: "8c0122a2-e544-11eb-ba80-0242ac130004"
  #                   userlikes: 3

  #                 )
  #                 {
  #                   success
  #                 }
  #               }
  #                """)
  #     assert resp.status_code == 200
  #     assert resp.body["data"]["createComment"]["success"] == true

  # end

  # test "Create reply", %{conn: _conn} do
  #   {:ok, resp} =
  #     Neuron.query("""
  #               mutation{
  #                 createReply(
  #                   commentid: "8c0122a2-e544-11eb-ba80-0242ac130004"
  #                   isdeletedbyadmin: true
  #                   likecount: 4
  #                   mediaUrl: " "
  #                   postid: "8c0122a2-e544-11eb-ba80-0242ac130004"
  #                   reply: "test"
  #                   status: 1
  #                   userid: "8c0122a2-e544-11eb-ba80-0242ac130004"
  #                   userlikes: 4
  #                 )
  #                 {
  #                   success
  #                 }
  #               }
  #                """)

  #     assert resp.status_code == 200
  #     assert resp.body["data"]["createReply"]["success"] == true

  # end

  test "Get comment", %{conn: _conn} do
    {:ok, resp} =
      Neuron.query("""
                query{
                  getComments(postid: "8c0122a2-e544-11eb-ba80-0242ac130004" ){
                    comments
                    id
                  }
                }
                 """)
      assert resp.status_code == 200

  end


  test "Get reply", %{conn: _conn, comment_id: comment_id} do
    {:ok, resp} =
      Neuron.query("""
                mutation{
                  getRepyId(commentid: "#{comment_id}")
                  {
                    reply
                  }
                }
                 """)
      assert resp.status_code == 200

  end

  test "Update Comment", %{conn: _conn, comment_id: comment_id} do
    {:ok, resp} =
      Neuron.query("""
                mutation{
                  updateComment(
                    comments: "updated"
                    id: "#{comment_id}"
                    isdeletedbyadmin: false
                    likecount: 4
                    mediaUrl: " "
                    postid: "8c0122a2-e544-11eb-ba80-0242ac130004"
                    status: 2
                    userid: "8c0122a2-e544-11eb-ba80-0242ac130004"
                    userlikes: 4
                  ) {
                    success
                  }
                }
                 """)

      assert resp.status_code == 200
      assert resp.body["data"]["updateComment"]["success"] == true

  end

  test "Update Reply", %{conn: _conn, reply_id_inserted: reply_id_inserted, comment_id: comment_id} do
    {:ok, resp} =
      Neuron.query("""
                    mutation{
                      updateReply(
                        commentid: "#{comment_id}"
                        id: "#{reply_id_inserted}"
                        isdeletedbyadmin:true
                        likecount:4
                        mediaUrl: " "
                        postid: "8c0122a2-e544-11eb-ba80-0242ac130004"
                        reply: "updated 2"
                        status: 1
                        userid: "8c0122a2-e544-11eb-ba80-0242ac130004"
                        userlikes:4
                      )
                      {
                        success
                      }
                    }
                 """)

      assert resp.status_code == 200
      assert resp.body["data"]["updateReply"]["success"] == true

  end

  test "Delete comment", %{conn: _conn,  comment_id: comment_id} do
    {:ok, resp} =
      Neuron.query(
        """
                mutation{
                  deleteComment(commentid:  "#{comment_id}")
                  {
                    success
                  }
                }
                 """
                 )

      assert resp.status_code == 200
      assert resp.body["data"]["deleteComment"]["success"] == true

  end

  test "Delete reply", %{conn: _conn, reply_id_inserted: reply_id_inserted} do
    {:ok, resp} =
      Neuron.query(
        """
              mutation{
                deleteReply(id: "#{reply_id_inserted}" )
                {
                  success
                }
              }
                 """
                 )

      assert resp.status_code == 200
      assert resp.body["data"]["deleteReply"]["success"] == true

  end

end
