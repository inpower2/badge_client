defmodule BadgeClientWeb.Router do
  use BadgeClientWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers

  end

  # scope "/" do
  #   pipe_through :browser

  #   forward "/graphiql",
  #             Absinthe.Plug.GraphiQL,
  #             schema: BadgeGraphql.GraphQL.Schema,
  #             interface: :playground,
  #             default_url: "/badges"
  # end
  scope "/" do
    pipe_through :api
    forward("/graphql", Absinthe.Plug, schema: BadgeGraphql.GraphQL.Schema)
    forward("/graphiql", Absinthe.Plug.GraphiQL, schema: BadgeGraphql.GraphQL.Schema)
  end


  scope "/api", BadgeClientWeb do
    pipe_through :api
  end
end
