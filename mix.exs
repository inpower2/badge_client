defmodule BadgeClient.MixProject do
  use Mix.Project

  def project do
    [
      app: :badge_client,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {BadgeClient.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.7"},
      {:phoenix_pubsub, "~> 1.1"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:absinthe_plug, "~> 1.5"},
      {:cowboy, "< 2.8.0", override: true},
      {:cowlib, "~> 2.9.0", override: true},
      # {:badge_proto, path: "../badge_proto"}, # for local use
      {:badge_proto, git: "https://bitbucket.org/inpower2/badge_proto.git"}, # for prod use
      {:plug_cowboy, "~> 2.0"}
    ]
  end
end
